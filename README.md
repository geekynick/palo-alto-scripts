# palo-alto-scripts

A collection of scripts for working with Palo Alto firewalls

## api folder

A collection of scripts using the XML API.

## cli folder

A collection of scripts to generate CLI commands.

## pandevice folder

A collection of scripts using the pandevice framework.

## parse_last_pushed.py

Takes a last-pushed-sp-config.xml file, and exports the rules in the post security rulebase in CSV format.

See api/policy.py for a better tool if you have API access.

## parse_panorama.py

Takes a panorama.xml file and exports the rules in the post security rulebase in CSV format.

See api/policy.py for a better tool if you have API access.
