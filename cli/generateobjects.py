"""Generate the CLI commands to add a bunch of addresses and then
add them all to a group"""


with open('ips.txt', 'r') as iplistfile:
    iplist = iplistfile.readlines()

objectnames = []
for num, ip in enumerate(iplist):
    print('set address ocsp_{} ip-netmask {}'.format(num + 1, ip))
    objectnames.append('ocsp_' + str(num + 1))

print('set address-group OCSP static [ {} ]'.format(' '.join(objectnames)))
