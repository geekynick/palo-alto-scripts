# Palo Alto Scripts - CLI

A collection of scripts to generate CLI commands for Palo Alto.

## generateobjects.py

Takes an input list of IP addresses, and outputs the commands to create and object for each one, and add them to a group.

Could do with being smarter, as it takes a fixed filename and uses a fixed prefix, but it was written in a hurry for what I needed.

Example in this folder adds OCSP (certificate revocation) IP addresses.

Usage:
`python generateobjects.py`