#!/usr/bin/env python3
'''
Takes a lastpush config, and prints a CSV of the post-rulebase/security/rules/
'''

import xml.etree.ElementTree as etree

tree = etree.parse('last-pushed-sp-config.xml')

rules = []


def flatten(l):
    thisbit = ''
    for entry in l:
        thisbit += entry
        thisbit += ';'
    return thisbit

# for vsys in tree.findall('./vsys/'):
#  print(vsys.attrib['name'])


for vsys in tree.findall('./vsys/'):
    # print(vsys.attrib['name'])
    for rule in vsys.findall('./post-rulebase/security/rules/'):
        thisrule = {}
        thisrule['vsys'] = vsys.attrib['name']
        thisrule['name'] = rule.attrib['name']
        thisrule['to'] = []
        for member in rule.findall('to/'):
            thisrule['to'].append(member.text)
        thisrule['from'] = []
        for member in rule.findall('from/'):
            thisrule['from'].append(member.text)
        thisrule['source'] = []
        for member in rule.findall('source/'):
            thisrule['source'].append(member.text)
        thisrule['destination'] = []
        for member in rule.findall('destination/'):
            thisrule['destination'].append(member.text)
        thisrule['application'] = []
        for member in rule.findall('application/'):
            thisrule['application'].append(member.text)
        thisrule['service'] = []
        for member in rule.findall('service/'):
            thisrule['service'].append(member.text)
        rules.append(thisrule)

with open('lastpush.csv', 'w') as f:
    f.write('vsys,name,to,from,source,destination,application,service\n')
    for rule in rules:
        thisline = rule['vsys'] + ','
        thisline += rule['name'] + ','
        thisline += flatten(rule['to']) + ','
        thisline += flatten(rule['from']) + ','
        thisline += flatten(rule['source']) + ','
        thisline += flatten(rule['destination']) + ','
        thisline += flatten(rule['application']) + ','
        thisline += flatten(rule['service'])
        f.write(thisline + '\n')
