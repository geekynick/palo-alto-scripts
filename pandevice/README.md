# Palo Alto Scripts - pandevice

A collection of scripts using pandevice: https://github.com/PaloAltoNetworks/pandevice

## checksecurityprofiles.py

Return a list of assigned security profiles per rule in the policy.