#!/usr/bin/env python3
"""
Script to check what security profile is applied to every rule
"""

import getpass
from pandevice import panorama
import pandevice


def printsecuritypolicies(pan, devicegroup):
    """Prints the security policy applied to every security rule
       in the post-rulebase for a given device group
       pan: pandevice.panorama.Panorama
       devicegroup: pandevice.panorama.DeviceGroup
    """
    pan.add(devicegroup)
    postrulebase = panorama.policies.PostRulebase()
    devicegroup.add(postrulebase)
    rules = panorama.policies.SecurityRule.refreshall(postrulebase)
    for rule in rules:
        print('{}, {}'.format(rule.name, rule.group))


if __name__ == '__main__':
    server = input('Enter Panorama FQDN/IP: ')
    user = input('Enter username with API access: ')
    password = getpass.getpass('Enter password: ')
    pan = panorama.Panorama(server, user, password)
    dg_list = panorama.DeviceGroup.refreshall(pan)
    for dg in dg_list:
        print(dg.name)
        print('==================================')
        printsecuritypolicies(pan, dg)
        print('\n\n\n')
