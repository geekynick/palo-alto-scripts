#!/usr/bin/env python3
'''
Check logs for a list of specific rule names for hits in the last 24 hours
'''

import argparse
import os
import requests
import smtplib
import sys
import time

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from requests.packages.urllib3.exceptions import (  # pylint: disable=E0401
    InsecureRequestWarning
)
from xml.etree import ElementTree


class JobNotFinishedError(Exception):
    '''Class for when a job hasn't finished in an acceptable time'''
    pass


def setarguments():
    """set up command line arguments. Returns args"""
    parser = argparse.ArgumentParser('Check Palo Logs')
    parser.add_argument('credentialfile',
                        help='Path to the credential file')
    parser.add_argument('inputfile',
                        help="A file containing a list of rule names "
                        "(one per line ""rulename,vsys"")")
    return parser.parse_args()


def initiateTrafficLogSearch(server, key, query, nlogs, proxies=None):
    '''
    Initiate a traffic log search
    '''
    requests.packages.urllib3.disable_warnings(  # pylint: disable=E1101
        InsecureRequestWarning
    )
    baseURL = 'https://' + server + '/api/'
    payload = {
        'type': 'log',
        'log-type': 'traffic',
        'query': query,
        'nlogs': str(nlogs),
        'key': key
    }
    resp = requests.get(baseURL,
                        params=payload,
                        proxies=proxies,
                        verify=False)
    resp.raise_for_status()
    tree = ElementTree.fromstring(resp.content)
    return tree.find('result/job').text


def getResultsForJobID(server, key, jobid, proxies=None):
    '''
    Get search results for a job ID
    '''
    requests.packages.urllib3.disable_warnings(  # pylint: disable=E1101
        InsecureRequestWarning
    )
    baseURL = 'https://' + server + '/api/'
    payload = {
        'type': 'log',
        'action': 'get',
        'job-id': jobid,
        'key': key
    }
    loopcount = 0
    while loopcount < 120:
        resp = requests.get(baseURL,
                            params=payload,
                            proxies=proxies,
                            verify=False)
        resp.raise_for_status()
        tree = ElementTree.fromstring(resp.content)
        if tree.find('result/job/status') is not None:
            if tree.find('result/job/status').text == 'FIN':
                # If the job is complete, quit the loop
                break
        time.sleep(5)
        loopcount += 1
    else:
        raise JobNotFinishedError
    return tree.findall('result/log/logs/entry')


def getLastHitTime(server, key, rulename, vsys):
    '''
    Find the last time a rule was hit, in the last 30 days.
    '''
    # query = "((rule eq '{}') AND " \
    #         "( vsys eq {}) AND " \
    #         "( receive_time in last-30-days))".format(rulename, vsys)
    query = "((rule eq '{}') AND " \
            "( vsys eq {}))".format(rulename, vsys)
    jobid = initiateTrafficLogSearch(
        server,
        key,
        query,
        1
    )
    try:
        result = getResultsForJobID(server, key, jobid)
        if result:
            return result[0].find('time_generated').text
        else:
            return None
    except JobNotFinishedError:
        return "Search job didn't finish in time"


def sendMail(results, lasthits):
    '''Send the results out'''
    sender = 'xxx@xxx.com'
    recipients = [
        'xxx@xxx.com',
    ]
    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'App IDs report'
    msg['From'] = sender
    msg['To'] = ', '.join(recipients)
    text = ''
    html = """\
        <html>
            <head></head>
            <body>
                <p>Below are the app ID's using the rules in the last 24h \
                (max 5000 logs searched)</p>
                <table border="1" width="600">
                    <tr>
                        <th>Rule Name</th>
                        <th>Vsys</th>
                        <th>App IDs</th>
                        <th>Last Hit</th>
                    </tr>
           """
    for key in sorted(results):
        text += '{}: {}: {}: {}\n'.format(
            key.split(',')[0],
            key.split(',')[1],
            ', '.join(results[key]) or 'No Hits',
            lasthits[key]
        )
        html += '<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>'.format(
            key.split(',')[0],
            key.split(',')[1],
            ', '.join(results[key]) or 'No Hits',
            lasthits[key]
        )

    html += """\
                    </table>
                </body>
            </html>
           """

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    msg.attach(part1)
    msg.attach(part2)

    s = smtplib.SMTP('mail.xxx.com')
    s.sendmail(sender, recipients, msg.as_string())


def findAllAppIDs(logs):
    """Find all app ID's in a list of log entries"""
    appids = set()
    for log in logs:
        appids.add(log.find('app').text)
    return sorted(appids)


if __name__ == '__main__':
    results = {}
    lasthits = {}
    args = setarguments()
    try:
        with open(args.inputfile, 'r') as rulenamesfile:
            rulelist = rulenamesfile.readlines()
        with open(args.credentialfile, 'r') as credfile:
            credfilecontents = credfile.readlines()
            server = credfilecontents[0].strip()
            key = credfilecontents[1].strip()
    except FileNotFoundError as err:
        print(err)
        print('File not found. Exiting.')
        sys.exit()
    except IndexError as err:
        print('Invalid credential file. Exiting')
        sys.exit()
    # Remove blank lines in the list and strip each line
    rulelist = [line.strip() for line in rulelist if line.strip()]
    for ruleline in rulelist:
        rule, vsys = ruleline.split(',')
        query = "((rule eq '{}') AND " \
                "( receive_time in last-24-hrs))".format(rule)
        try:
            jobid = initiateTrafficLogSearch(server,
                                             key,
                                             query,
                                             '5000')
            # Give it 5 seconds to make sure the job started.
            time.sleep(5)
            logs = getResultsForJobID(server, key, jobid)
            appids = findAllAppIDs(logs)
            results[ruleline] = appids
        except AttributeError:
            results[ruleline] = "Invalid search query"
        except JobNotFinishedError:
            results[ruleline] = "Query didn't finish in an acceptable time"

        lasthit = getLastHitTime(
            server,
            key,
            rule,
            vsys
        )
        lasthits[ruleline] = lasthit or 'No hits found'

    sendMail(results, lasthits)
    # print(results)
    # print(lasthits)
