#!/usr/bin/env python3
'''
Export yesterday's Palo Logs
'''

import argparse
import os
import requests
import smtplib
import sys
import time

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from requests.packages.urllib3.exceptions import (  # pylint: disable=E0401
    InsecureRequestWarning
)
from xml.etree import ElementTree


class JobNotFinishedError(Exception):
    '''Class for when a job hasn't finished in an acceptable time'''
    pass


def setarguments():
    """set up command line arguments. Returns args"""
    parser = argparse.ArgumentParser("Export yesterday's Palo Logs")
    parser.add_argument('credentialfile',
                        help='Path to the credential file')
    return parser.parse_args()


def initiateTrafficLogSearch(server, key, query, nlogs, skip, proxies=None):
    '''
    Initiate a traffic log search
    '''
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    baseURL = 'https://' + server + '/api/'
    payload = {
        'type': 'log',
        'log-type': 'traffic',
        'query': query,
        'nlogs': str(nlogs),
        'key': key,
        'skip': str(skip)
    }
    resp = requests.get(baseURL,
                        params=payload,
                        proxies=proxies,
                        verify=False)
    resp.raise_for_status()
    tree = ElementTree.fromstring(resp.content)
    return tree.find('result/job').text


def getResultsForJobID(server, key, jobid, proxies=None):
    '''
    Get search results for a job ID
    '''
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    baseURL = 'https://' + server + '/api/'
    payload = {
        'type': 'log',
        'action': 'get',
        'job-id': jobid,
        'key': key
    }
    loopcount = 0
    # try for 30 seconds - if the job isn't completed consider it dead
    while loopcount < 120:
        resp = requests.get(baseURL,
                            params=payload,
                            proxies=proxies,
                            verify=False)
        resp.raise_for_status()
        tree = ElementTree.fromstring(resp.content)
        if tree.find('result/job/status').text == 'FIN':
            # If the job is complete, quit the loop
            break
        else:
            # if it's not finished, give it a second
            time.sleep(1)
            loopcount += 1
    else:
        raise JobNotFinishedError
    return tree.findall('result/log/logs/entry')


if __name__ == '__main__':
    args = setarguments()
    try:
        with open(args.credentialfile, 'r') as credfile:
            credfilecontents = credfile.readlines()
            server = credfilecontents[0].strip()
            key = credfilecontents[1].strip()
    except FileNotFoundError as err:
        print(err)
        print('File not found. Exiting.')
        sys.exit()
    except IndexError as err:
        print('Invalid credential file. Exiting')
        sys.exit()

    query = "(time_generated leq '2018/03/22 23:59:59') and " \
            "(time_generated geq '2018/03/22 00:00:01')"
    logs = ['First']
    results = []
    try:
        while len(logs) > 0:
            jobid = initiateTrafficLogSearch(server,
                                             key,
                                             query,
                                             '5000',
                                             len(results))
            # Give it 5 seconds to make sure the job started.
            time.sleep(1)
            logs = getResultsForJobID(server, key, jobid)
            results += logs
            print('{} log entries'.format(len(results)))
            print('Earliest entry: {}'.format(
                logs[-1].find('.//time_generated').text)
            )
    except AttributeError:
        print("Invalid search query")
    except JobNotFinishedError:
        print("Query didn't finish in an acceptable time")
