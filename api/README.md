# Palo Alto Scripts - API

This is a collection of scripts making use of the Palo Alto API.

## creds.py

Interactively asks for the address of the firewall / panorama, and a username and password, then goes off and gets an API key, saving it to a credential file.

Most other scripts will ask for this credential file as input.

The file is simply two lines - the server name on the first line, and the API key on the second line.

Usage:
`python3 creds.py`

## findzone.py

Finds a zone for a given virtual router and IP address by examining the routing table to find the exit interface, and then looking up the interface to find the zone.

Usage:
`python3 findzone.py virtualrouter ipaddr credentialfile`

## logs.py and logexport.py

EXPERIMENTAL.

They are used to export logs for specific time periods, and check last hit counts etc.

I've had mixed results using these, and they are working scripts, so use at your own peril. I've only included them in the repo in case the code is useful to anyone.

## policy.py

Download a copy of a policy for a given vsys, from Panorama. Remove a couple of XML tags, and then save it somewhere.

I use this to then import to Excel as a datasource - gives a useful export of firewall rules, but this script removes a few tags which break Excel.