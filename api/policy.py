#!/usr/bin/env python3
'''
Download the policy for a specific vsys
'''

import argparse
import requests
import sys
from requests.packages.urllib3.exceptions import (  # pylint: disable=E0401
    InsecureRequestWarning
)


def setarguments():
    """set up command line arguments. Returns args"""
    parser = argparse.ArgumentParser('Download policy for single '
                                     'vsys from Panorama.')
    parser.add_argument('credentialfile',
                        help='Path to the credential file')
    parser.add_argument('vsys',
                        help="The vsys name (e.g. ENT-FW001-NOR) ")
    return parser.parse_args()


def downloadconfig(server, key, vsys):
    """
    Download the config file, and configure it for an Excel data import
    """
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    baseURL = 'https://' + server + '/api/'
    payload = {
              'type': 'config',
              'action': 'get',
              'xpath': "/config/devices/entry[@name='localhost.localdomain']"
                       "/device-group/entry[@name='" + vsys +
                       "']/post-rulebase/security",
              'key': key
              }
    resp = requests.get(baseURL,
                        params=payload,
                        proxies=None,
                        verify=False)
    resp.raise_for_status()
    return resp.text

if __name__ == '__main__':
    results = {}
    lasthits = {}
    args = setarguments()
    try:
        with open(args.credentialfile, 'r') as credfile:
            credfilecontents = credfile.readlines()
            server = credfilecontents[0].strip()
            key = credfilecontents[1].strip()
    except FileNotFoundError as err:
        print(err)
        print('File not found. Exiting.')
        sys.exit()
    except IndexError as err:
        print('Invalid credential file. Exiting')
        sys.exit()
    config = downloadconfig(server, key, args.vsys)
    config = config.replace('  ', '')
    config = config.replace('<member>', '')
    config = config.replace('</member>', '')
    config = "\n".join(config.split('\n')[1:-1])
    with open('source.xml', 'w') as outfile:
        outfile.write(config)
